## Multiprotocol web chat client project

DOUBTS
===================
> - How to kill the irc_client (proxy) when WebClient exits?
> - Estamos usando AMQP para preserver as mensagens mesmo que o cliente não esteja conectado.  
Contudo, se encerro o 'irc-proxy.js' o comportamento ao executá-lo novamente não é o esperado.  
A mensagem de fato é consumida na queue, porém, o objeto 'irc_client' já não existe.  
Tal objeto é criado consumindo da queue 'registro_de_conexão', mas o publisher não realimentará a queue neste caso.  
Como resolver?

INITIAL OBJECTIVES
===================
## Primary
> - Start with *MOTD* (like in class);
> - Fix nicks problem;
	* Checking nick before connection;
	* Setting new nick if already taken(?);
	* Informing client of nick change and reason.
> - Private messaging (new "_tab_");

## Secondary
> - Tailoring chat funcionalities:
	* Cleaning up message text field after send;
	* Send through Return/Enter (no button);
	* Chat dimensions;
> - Putting code in english:
	* Function names and everything else;
	* Proper comments;
	* Build user and developer's manual;
