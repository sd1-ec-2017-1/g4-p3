var express = require('express');		//express module
var app = express();				//express object
var bodyParser = require('body-parser');	//process request body
var cookieParser = require('cookie-parser');	//process cookies
var path = require('path');			//file path
var amqp = require('amqplib/callback_api');	//ampq com
var server = require('http').createServer(app);	//http server necessary for socket.io
var io = require('socket.io').listen(server);	//assign socket.io to server
var Hashtable = require('hashtable');		//include hashtable lib

//Create new Proxies Hashtable
proxyHashtable = new Hashtable();

//Set up express
app.use(bodyParser.json());
app.use(bodyParser.urlencoded( { extended: true } ));
app.use(cookieParser());
app.use(express.static('public'));

var id_gen = 0; // ID generator
var users = {}; // Users array

var amqp_conn;
var amqp_ch;


//Establish connection with AMPQ server
amqp.connect('amqp://localhost', function(err, conn)
{

	conn.createChannel(function(err, ch)
	{
		amqp_conn = conn;
		amqp_ch = ch;
	});
});


function sendToServer(comando, msg)
{

	msg = new Buffer(JSON.stringify(msg));

	amqp_ch.assertQueue(comando, {durable: false});
	amqp_ch.sendToQueue(comando, msg);
	console.log(" [app] Sent %s", msg);
}

function receberDoServidor (id, callback)
{

	amqp_ch.assertQueue("user_"+id, {durable: false});

	console.log(" [app] Waiting messages for "+ id);

	amqp_ch.consume("user_"+id, function(msg)
	{
		console.log(" [app] Received %s", msg.content.toString());
		callback(JSON.parse(msg.content.toString()));
	}, {noAck: true});
}

//Process login and store data through cookies
app.post('/login', function (req, res)
{ 
	res.cookie('nick', req.body.nome);
	res.cookie('canal', req.body.canal);
	res.cookie('servidor', req.body.servidor);
	res.redirect('/');
});

//Register connection with IRC server
app.get('/', function (req, res)
{

	if ( req.cookies.servidor && req.cookies.nick  && req.cookies.canal )
	{
		id_gen++; // Cria um ID para o usuário
		id = id_gen;

		// Cria um cache de mensagens
		users[id] = {cache: [{
		"timestamp": Date.now(), 
		"nick": "IRC Server",
		"msg": "Welcome to the IRC server"}]}; 

		res.cookie('id', id); // Seta o ID nos cookies do cliente

		var target = 'registro_conexao';
		var msg = {
		id: id, 
		servidor: req.cookies.servidor,
		nick: req.cookies.nick, 
		canal:"#"+ req.cookies.canal
		};

		users[id].id       = id;
		users[id].servidor = msg.servidor;
		users[id].nick     = msg.nick;
		users[id].canal    = msg.canal;

		// Envia registro de conexão para o servidor
		sendToServer(target, msg);

		// Se inscreve para receber mensagens endereçadas a este usuário
		receberDoServidor(id, function (msg) {

		// Adiciona mensagem ao cache do usuário
		users[id].cache.push(msg);
		});

		res.sendFile(path.join(__dirname, 'web-front/index.html'));
	}
	else
	{
		res.sendFile(path.join(__dirname, 'web-front/login.html'));
	}
});

// Obtém mensagens armazenadas em cache (via polling)
app.get('/obter_mensagem/:timestamp', function (req, res)
{

	var id = req.cookies.id;
	var response = users[id].cache;
	users.cache = [];

	res.append('Content-type', 'application/json');
	res.send(response);
});

// Envia uma mensagem para o servidor AMQP que sera consumida pelo irc-proxy e enviada para o servidor IRC
app.post('/gravar_mensagem', function (req, res)
{
	// Adiciona mensagem enviada ao cache do usuário
	users[req.cookies.id].cache.push(req.body);

	sendToServer("gravar_mensagem",
	{
		canal: users[req.cookies.id].canal, 
		msg: req.body.msg
	});

	res.end();
});

server.listen(3000, function ()
{
	console.log('Example app listening on port 3000!');
});

io.on('connection', function(socket)
{
	console.log("[SYSTEM]: Connection made");
	//Manual Cookie parsing
	var re = /\s*;\s*/;
	var buffer = socket.request.headers.cookie.split(re);
	var id;
	var channel;
	var server;
	var nick;
	
	//Parsing Cookie Routine
	buffer.forEach(function(data)
	{
		
		if(data.startsWith('id='))
		{
			id = parseInt(data[3], 10);
			console.log("ID = " + id);
			//proxies[number].irc_client.say('#canal', msg);
		}
		if(data.startsWith('canal='))
		{
			canal = '#'+data.slice(6);
			console.log('CHANNEL = ' + canal);
		}
		if(data.startsWith('nick='))
		{
			nick = data.slice(5);
			console.log('NICK = ' + nick);
		}
		if(data.startsWith('servidor'))
		{
			servidor = data.slice(9);
			console.log('SERVER = ' + servidor);
		}
	});

	socket.on('message', function(msg)
	{

		users[id].cache.push(msg);

		sendToServer("gravar_mensagem",
		{
			canal: users[id].canal, 
			msg: msg
		});
	});

	//Checking nick
	//if(proxyHashtable.has(nick)) socket.emit('!nick');
});
